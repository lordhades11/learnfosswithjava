/* main.java
 *
 * Copyright 2019 Gaurav Agrawal <agrawalgaurav1999@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


package org.gaurav.learnjava;

import org.gaurav.learnjava.BinaryCalculator;
import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		System.out.println ("This is a basic calculator\n");
		System.out.println ("Avaiable operations are\n");
		System.out.println ("1.Binary Calculator\n");
		//TODO:Add other avaialable operations
		Scanner input = new Scanner (System.in);
		int choice_operator = input.nextInt ();

		switch (choice_operator) {

			case 1:
					BinaryCalculator calculator = new BinaryCalculator ();
					System.out.println ("Avaialable operations are\n");
					System.out.println ("1. Addition\n");
					//TODO: Add other available binary operation
					int binary_operation = input.nextInt ();
					switch (binary_operation) {

						case 1: {
									System.out.println ("Enter number 1\n");
									int number1 = input.nextInt ();
									System.out.println ("Enter number 2\n");
									int number2 = input.nextInt ();
									System.out.println ("Sum is:-"+calculator.get_sum (number1, number2));
									break;
							}
						//TODO: Add other cases for sub, div, mul etc.

						default: {
									System.out.println ("Deafult case ;)");
									break;
							}

					}
					break; //Case 1 breaks here
			//TODO: Add other operators here for example maybe ternary , unary etc.
			default:
					System.out.println ("Default operator case ;");
					break;

		}
	}

}
