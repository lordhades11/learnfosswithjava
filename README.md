# LearnFossWithJava

## Description
Learn foss with Java is a project aimed at students for learning Open source practices using this pet project
It will cover things like
- Git
- Build systems
- Coding style
- Conventions
- Code reviews

## Project
A basic console calculator written in Java

## Build system
```
https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html

```
## Execution [Inside project]
```
$cd build/jar
$java -jar Calculator.jar

```